/*
Реализовать функцию, принимающую на вход массив и целое число.
Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.

Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20

стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0

 */
public class Program {
    public static void main(String[] args) {
        int[] testArray = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(returnIndexInArrayByInt(testArray, 18));
        printArray(testArray);
        moveZerosInArrayToTheRight(testArray);
        printArray(testArray);
    }

    public static int returnIndexInArrayByInt (int[] arrayOfInts, int intToFind){

        for (int i = 0; i < arrayOfInts.length; i++) {
            if (intToFind == arrayOfInts[i])
                return i;
        }
        return -1;
    }

    public static void moveZerosInArrayToTheRight(int[] arrayToDo){
        int[] tempArray = new int[arrayToDo.length];
        int j = 0;

        for (int i = 0; i < arrayToDo.length; i++) {
            if (arrayToDo[i] != 0)
                tempArray[j++] = arrayToDo[i];
        }
        for (int i = 0; i < arrayToDo.length; i++) {
            arrayToDo[i] = tempArray[i];
        }
    }

    public static void printArray(int[] arrayToPrint) {
        System.out.print("Array contains: { ");
        for (int element : arrayToPrint){
            System.out.print(element+ " ");
        }
        System.out.println("}");
    }
}
