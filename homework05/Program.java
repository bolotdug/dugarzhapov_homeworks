import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
Реализовать программу на Java, которая для последовательности чисел,
оканчивающихся на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

Например:
345
298
456
-1
Ответ: 2
 */
public class Program {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String line = "";
        String tempLine = reader.readLine();
        while (true){
            if (tempLine.equals("-1"))
                break;
            line += tempLine;
            tempLine = reader.readLine();
        }

        System.out.println(minimalDigitInString(line));
    }

    public static String minimalDigitInString(String line){
        if (line.contains("0"))
            return "0";
        else if (line.contains("1"))
            return "1";
        else if (line.contains("2"))
            return "2";
        else if (line.contains("3"))
            return "3";
        else if (line.contains("4"))
            return "4";
        else if (line.contains("5"))
            return "5";
        else if (line.contains("6"))
            return "6";
        else if (line.contains("7"))
            return "7";
        else if (line.contains("8"))
            return "8";
        else if (line.contains("9"))
            return "9";
        else
            return "1, but you didn't entered any numbers before -1";
    }
}
